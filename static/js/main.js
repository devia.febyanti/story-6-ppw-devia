var accordion = document.getElementsByClassName("accordion");

for (var i = 0; i < accordion.length; i++) {
    accordion[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var elemen = this.nextElementSibling;
        if (elemen.style.maxHeight) {
            elemen.style.maxHeight = null;
        }
        else {
        elemen.style.maxHeight = elemen.scrollHeight + "px";
        } 
    });
}  

function move_up() {
  $(event.currentTarget.parentNode.previousElementSibling).insertAfter($(event.currentTarget.parentNode))
}

function move_down() {
  $(event.currentTarget.parentNode).insertAfter($(event.currentTarget.parentNode.nextElementSibling))
}